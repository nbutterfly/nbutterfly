import 'package:flutter/material.dart';
import 'package:graphic/graphic.dart';

class DetailHomeScreen extends StatefulWidget {
  DetailHomeScreen({Key key}) : super(key: key);

  @override
  _DetailHomeScreenState createState() => _DetailHomeScreenState();
}

class _DetailHomeScreenState extends State<DetailHomeScreen> {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('C-19 Risk At-a-Glance'),
      ),
      //hit Ctrl+space in intellij to know what are the options you can use in flutter widgets
      body: new Container(
        padding: new EdgeInsets.all(12.0),
        child: new Center(
          child: new Column(
            children: [
              _printComfort(),
              _plotBarChart(),
              _plotLineChart(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _printComfort() {
    final comfortInfo = Column(
      children: [
        Text(
          'Event Comfort',
          style: statusStyle,
        ),
        Text(
          '670',
          style: statusStyle,
        ),
      ],
    );

    return Container(
      //padding: EdgeInsets.only(top: 10),
      child: comfortInfo,
    );
  }

  Widget _plotBarChart() {
    final statusInfo = Column(
      children: [
        Text(
          'Bar Chart',
          style: statusStyle,
        ),
      ],
    );

    return Container(
      //padding: EdgeInsets.only(top: 90),
      child: statusInfo,
    );
  }

  Widget _plotLineChart() {
    final statusInfo = Column(
      children: [
        Text(
          'Line Chart',
          style: statusStyle,
        ),
      ],
    );

    return Container(
      //padding: EdgeInsets.only(top: 90),
      child: statusInfo,
    );
  }

  // Styles

  final statusStyle = TextStyle(
    color: Colors.blueGrey,
    fontWeight: FontWeight.w800,
    fontFamily: 'Roboto',
    letterSpacing: 0.5,
    fontSize: 35,
    //height: 2,
  );
}
