import 'package:flutter/material.dart';
import 'package:graphic/graphic.dart' as graphic;

import 'data.dart';

class DataViz extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('C-19 Risk At-a-Glance'),
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: <Widget>[
              Padding(
                child: Text('Comfort - 734 ',
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: Colors.blue[600],
                    )),
                padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
              ),
              Padding(
                child: Text('Risk Status', style: TextStyle(fontSize: 20)),
                padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
              ),
              Container(
                width: 350,
                height: 300,
                child: graphic.Chart(
                  data: basicData,
                  scales: {
                    'genre': graphic.CatScale(
                      accessor: (map) => map['genre'] as String,
                    ),
                    'sold': graphic.LinearScale(
                      accessor: (map) => map['sold'] as num,
                      nice: true,
                    )
                  },
                  geoms: [
                    graphic.IntervalGeom(
                      position: graphic.PositionAttr(field: 'genre*sold'),
                      shape: graphic.ShapeAttr(values: [
                        graphic.RectShape(radius: Radius.circular(5))
                      ]),
                    )
                  ],
                  axes: {
                    'genre': graphic.Defaults.horizontalAxis,
                    'sold': graphic.Defaults.verticalAxis,
                  },
                ),
              ),
              Padding(
                child: Text('Risk Timeline', style: TextStyle(fontSize: 20)),
                padding: EdgeInsets.fromLTRB(0, 40, 0, 0),
              ),
              Container(
                width: 350,
                height: 300,
                child: graphic.Chart(
                  data: adjustData,
                  scales: {
                    'index': graphic.CatScale(
                      accessor: (map) => map['index'].toString(),
                      range: [0, 1],
                    ),
                    'type': graphic.CatScale(
                      accessor: (map) => map['type'] as String,
                    ),
                    'value': graphic.LinearScale(
                      accessor: (map) => map['value'] as num,
                      nice: true,
                    ),
                  },
                  geoms: [
                    graphic.LineGeom(
                      position: graphic.PositionAttr(field: 'index*value'),
                      color: graphic.ColorAttr(field: 'type'),
                      shape: graphic.ShapeAttr(
                          values: [graphic.BasicLineShape(smooth: true)]),
                    )
                  ],
                  axes: {
                    'index': graphic.Defaults.horizontalAxis,
                    'value': graphic.Defaults.verticalAxis,
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
