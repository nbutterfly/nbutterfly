import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:path/path.dart';
import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:date_format/date_format.dart';
import 'package:intl/intl.dart';
import 'package:uuid/uuid.dart';

class CreateNewEvent extends StatefulWidget {
  String uid;
  CreateNewEvent({this.uid});

  @override
  _CreateNewEventState createState() => _CreateNewEventState();
}

class _CreateNewEventState extends State<CreateNewEvent> {
  @override
  final GlobalKey<FormState> form = GlobalKey<FormState>();
  String _title,
      _time,
      _price,
      _place,
      _desc1,
      _desc2,
      _desc3,
      _date,
      _country,
      _category;

  TextEditingController titleController = new TextEditingController();
  TextEditingController timeController = new TextEditingController();
  TextEditingController priceController = new TextEditingController();
  TextEditingController placeController = new TextEditingController();
  TextEditingController imageController = new TextEditingController();
  TextEditingController countryController = new TextEditingController();
  TextEditingController categoryController = new TextEditingController();

  TextEditingController desc1Controller = new TextEditingController();

  String _setTime, _setDate;

  String _hour, _minute;

  String dateTime;

  DateTime selectedDate = DateTime.now();

  TimeOfDay selectedTime = TimeOfDay(hour: 00, minute: 00);

  TextEditingController _dateController = TextEditingController();
  TextEditingController _timeController = TextEditingController();

  var PicUrl;

  File _image;
  String filename;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
      print('Image Path $_image');
    });
  }

  Future uploadPic(BuildContext context) async {
    String fileName = basename(_image.path);
    StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child(fileName);
    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);
    var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
    PicUrl = dowurl.toString();
    setState(() {
      print(" Picture uploaded");
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text(' Picture Uploaded')));
    });
  }

  Future selectPhoto() async {
    await ImagePicker.pickImage(source: ImageSource.gallery).then((image) {
      setState(() {
        _image = image;
        filename = basename(_image.path);
        uploadImage();
      });
    });
  }

  Future uploadImage() async {
    StorageReference firebaseStorageRef =
        FirebaseStorage.instance.ref().child(filename);

    StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);

    var dowurl = await (await uploadTask.onComplete).ref.getDownloadURL();
    await uploadTask.onComplete;
    print('File Uploaded');
    PicUrl = dowurl.toString();
    setState(() {
      PicUrl = dowurl.toString();
    });
    print("download url = $PicUrl");
    return PicUrl;
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: selectedTime,
    );
    if (picked != null)
      setState(() {
        selectedTime = picked;
        _hour = selectedTime.hour.toString();
        _minute = selectedTime.minute.toString();
        _time = _hour + ' : ' + _minute;
        _timeController.text = _time;
        _timeController.text = formatDate(
            DateTime(2019, 08, 1, selectedTime.hour, selectedTime.minute),
            [hh, ':', nn, " ", am]).toString();
      });
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        initialDatePickerMode: DatePickerMode.day,
        firstDate: DateTime(2015),
        lastDate: DateTime(2101));
    if (picked != null)
      setState(() {
        selectedDate = picked;
        _dateController.text = DateFormat.yMd().format(selectedDate);
      });
  }

  Widget build(BuildContext context) {
    // Create uuid object
    var uuid = Uuid();

    // Generate a v4 (random) id
    var v4 = uuid.v4(); // -> '110ec58a-a0f2-4ac4-8393-c866d813b8d1'

    void addData() {
      Firestore.instance.runTransaction((Transaction transaction) async {
        Firestore.instance
            .collection("CreateEventUser")
            .document("user")
            .collection(widget.uid)
            .document(v4 + widget.uid)
            .setData({
          "image": PicUrl.toString(),
          "title": _title,
          "time": _timeController.text,
          "place": _place,
          "date": _dateController.text,
          "description": _desc1,
          "category": _category,
          "id": v4,
        });
      });
    }

    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          "Create New Event",
          style: TextStyle(fontFamily: "Sofia", fontWeight: FontWeight.w400),
        ),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: form,
          child: Padding(
            padding: const EdgeInsets.only(top: 20.0, right: 20.0, left: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _image == null
                    ? new Stack(
                        children: <Widget>[
                          Container(
                            height: 240.0,
                            width: double.infinity,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: NetworkImage(
                                        "https://www.generationsforpeace.org/wp-content/uploads/2018/03/empty.jpg"))),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 200.0),
                            child: Align(
                              alignment: Alignment.bottomRight,
                              child: InkWell(
                                onTap: () {
                                  selectPhoto();
                                },
                                child: Container(
                                  height: 55.0,
                                  width: 55.0,
                                  decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(50.0)),
                                    color: Colors.blueAccent,
                                  ),
                                  child: Center(
                                    child: Icon(
                                      Icons.camera_alt,
                                      color: Colors.white,
                                      size: 22.0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    : Container(
                        height: 240.0,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          image: DecorationImage(image: FileImage(_image)),
                        )),
                SizedBox(
                  height: 30.0,
                ),
                Text(
                  "Title",
                  style: TextStyle(
                      fontFamily: "Sofia",
                      fontWeight: FontWeight.w700,
                      fontSize: 16.5),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Theme(
                  data: ThemeData(accentColor: Colors.deepPurpleAccent),
                  child: TextFormField(
                    validator: (input) {
                      if (input.isEmpty) {
                        return 'Please input your title';
                      }
                    },
                    maxLines: 2,
                    onSaved: (input) => _title = input,
                    controller: titleController,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.words,
                    style: TextStyle(
                        fontFamily: "WorkSansSemiBold",
                        fontSize: 16.0,
                        color: Colors.black),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              width: 0.5,
                              color: Colors.black12.withOpacity(0.01))),
                      contentPadding: EdgeInsets.all(13.0),
                      hintText: "Input your title",
                      hintMaxLines: 2,
                      hintStyle: TextStyle(fontFamily: "Sans", fontSize: 15.0),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "Time",
                  style: TextStyle(
                      fontFamily: "Sofia",
                      fontWeight: FontWeight.w700,
                      fontSize: 16.5),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Theme(
                  data: ThemeData(accentColor: Colors.deepPurpleAccent),
                  child: InkWell(
                    onTap: () {
                      _selectTime(context);
                    },
                    child: Container(
                      //  margin: EdgeInsets.only(top: 30),
                      width: double.infinity,
                      height: 55.0,
                      alignment: Alignment.center,
                      //  decoration: BoxDecoration(color: Colors.grey[200]),
                      child: TextFormField(
                        style: TextStyle(
                          fontSize: 18,
                          fontFamily: "Sofia",
                        ),
                        textAlign: TextAlign.left,
                        onSaved: (String val) {
                          _setTime = val;
                        },
                        enabled: false,
                        keyboardType: TextInputType.text,
                        controller: _timeController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(13.0),
                          hintText: "Select Time",
                          hintStyle:
                              TextStyle(fontFamily: "Sofia", fontSize: 18.0),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 0.5,
                                  color: Colors.black12.withOpacity(0.1))),
                          // labelText: 'Time',
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "Place",
                  style: TextStyle(
                      fontFamily: "Sofia",
                      fontWeight: FontWeight.w700,
                      fontSize: 16.5),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Theme(
                  data: ThemeData(accentColor: Colors.deepPurpleAccent),
                  child: TextFormField(
                    validator: (input) {
                      if (input.isEmpty) {
                        return 'Please input your place';
                      }
                    },
                    maxLines: 2,
                    onSaved: (input) => _place = input,
                    controller: placeController,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.words,
                    style: TextStyle(
                        fontFamily: "WorkSansSemiBold",
                        fontSize: 16.0,
                        color: Colors.black),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              width: 0.5,
                              color: Colors.black12.withOpacity(0.01))),
                      contentPadding: EdgeInsets.all(13.0),
                      hintText: "Input your place",
                      hintMaxLines: 2,
                      hintStyle: TextStyle(fontFamily: "Sans", fontSize: 15.0),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "Date",
                  style: TextStyle(
                      fontFamily: "Sofia",
                      fontWeight: FontWeight.w700,
                      fontSize: 16.5),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Theme(
                  data: ThemeData(accentColor: Colors.deepPurpleAccent),
                  child: InkWell(
                    onTap: () {
                      _selectDate(context);
                    },
                    child: Container(
                      //  margin: EdgeInsets.only(top: 30),
                      width: double.infinity,
                      height: 55.0,
                      alignment: Alignment.center,
                      //  decoration: BoxDecoration(color: Colors.grey[200]),
                      child: TextFormField(
                        style: TextStyle(
                          fontSize: 18,
                          fontFamily: "Sofia",
                        ),
                        textAlign: TextAlign.left,
                        onSaved: (String val) {
                          _setTime = val;
                        },
                        enabled: false,
                        keyboardType: TextInputType.text,
                        controller: _dateController,
                        decoration: InputDecoration(
                          contentPadding: EdgeInsets.all(13.0),
                          hintText: "Select Date",
                          hintStyle:
                              TextStyle(fontFamily: "Sofia", fontSize: 18.0),
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  width: 0.5,
                                  color: Colors.black12.withOpacity(0.1))),
                          // labelText: 'Time',
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "Category",
                  style: TextStyle(
                      fontFamily: "Sofia",
                      fontWeight: FontWeight.w700,
                      fontSize: 16.5),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Theme(
                  data: ThemeData(accentColor: Colors.deepPurpleAccent),
                  child: TextFormField(
                    validator: (input) {
                      if (input.isEmpty) {
                        return 'Please input your category';
                      }
                    },
                    maxLines: 2,
                    onSaved: (input) => _category = input,
                    controller: categoryController,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.words,
                    style: TextStyle(
                        fontFamily: "WorkSansSemiBold",
                        fontSize: 16.0,
                        color: Colors.black),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              width: 0.5,
                              color: Colors.black12.withOpacity(0.01))),
                      contentPadding: EdgeInsets.all(13.0),
                      hintText: "Input your category",
                      hintMaxLines: 2,
                      hintStyle: TextStyle(fontFamily: "Sans", fontSize: 15.0),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                Text(
                  "Description",
                  style: TextStyle(
                      fontFamily: "Sofia",
                      fontWeight: FontWeight.w700,
                      fontSize: 16.5),
                ),
                SizedBox(
                  height: 5.0,
                ),
                Theme(
                  data: ThemeData(accentColor: Colors.deepPurpleAccent),
                  child: TextFormField(
                    validator: (input) {
                      if (input.isEmpty) {
                        return 'Please input your description';
                      }
                    },
                    maxLines: 8,
                    onSaved: (input) => _desc1 = input,
                    controller: desc1Controller,
                    keyboardType: TextInputType.text,
                    textCapitalization: TextCapitalization.words,
                    style: TextStyle(
                        fontFamily: "WorkSansSemiBold",
                        fontSize: 16.0,
                        color: Colors.black),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                          borderSide: BorderSide(
                              width: 0.5,
                              color: Colors.black12.withOpacity(0.01))),
                      contentPadding: EdgeInsets.all(13.0),
                      hintText: "Input your description",
                      hintMaxLines: 2,
                      hintStyle: TextStyle(fontFamily: "Sans", fontSize: 15.0),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30.0,
                ),
                InkWell(
                  onTap: () async {
                    // addData();
                    final formState = form.currentState;

                    if (formState.validate()) {
                      formState.save();
                      setState(() {
                        addData();

                        confirmDialog(context);
                      });

                      addData();
                    } else {
                      showDialog(
                          context: context,
                          builder: (BuildContext context) {
                            return AlertDialog(
                              title: Text("Error"),
                              content: Text("Please input your information"),
                              actions: <Widget>[
                                FlatButton(
                                  child: Text("Close"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                )
                              ],
                            );
                          });
                    }
                  },
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          left: 20.0, right: 20.0, bottom: 20.0),
                      child: Container(
                        height: 55.0,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          color: Colors.deepPurpleAccent,
                          borderRadius: BorderRadius.all(Radius.circular(50.0)),
                        ),
                        child: Center(
                          child: Text(
                            "Upload",
                            style: TextStyle(
                                fontFamily: "Sofia",
                                color: Colors.white,
                                fontSize: 17.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

void confirmDialog(BuildContext context) async {
  showDialog(
    context: context,
    barrierDismissible: true,
    child: SimpleDialog(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(left: 10.0),
                child: InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Icon(
                      Icons.close,
                      size: 30.0,
                    ))),
            SizedBox(
              width: 10.0,
            )
          ],
        ),
        Container(
            padding: EdgeInsets.only(top: 30.0, right: 60.0, left: 60.0),
            color: Colors.white,
            child: Icon(
              Icons.check_circle,
              size: 150.0,
              color: Colors.green,
            )),
        Center(
            child: Padding(
          padding: const EdgeInsets.only(top: 16.0),
          child: Text(
            "Succes",
            style: TextStyle(fontWeight: FontWeight.w700, fontSize: 22.0),
          ),
        )),
        Center(
            child: Padding(
          padding: const EdgeInsets.only(top: 30.0, bottom: 40.0),
          child: Text(
            "Create Event Succes",
            style: TextStyle(fontSize: 17.0),
          ),
        )),
      ],
    ),
  );
}
