import 'dart:async';
import 'package:connectivity/connectivity.dart';
import 'package:nButterfly/Screen/N1_Home/butterfly_home.dart';
import 'package:nButterfly/Screen/N1_Home/vitals.dart';
//import 'package:nButterfly/Screen/B1_Home/Home.dart';
//import 'package:nButterfly/Screen/B3_Manage_Event/B3_Manage_Event.dart';
import 'package:nButterfly/Screen/B4_Profile/B4_Profile.dart';
import 'package:flutter/material.dart';

import 'custom_nav_bar.dart';

class bottomNavBar extends StatefulWidget {
  String idUser;
  bottomNavBar({this.idUser});

  _bottomNavBarState createState() => _bottomNavBarState();
}

class _bottomNavBarState extends State<bottomNavBar> {
  int currentIndex = 0;
  bool _color = true;
  Widget callPage(int current) {
    switch (current) {
      case 0:
        return new Butterfly();
        break;
      case 2:
        return new VitalSignScreen();
        break;
      case 3:
        return new profile(
          uid: widget.idUser,
        );
        break;
      default:
        return new Butterfly();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: callPage(currentIndex),
      bottomNavigationBar: BottomNavigationDotBar(
          // Usar -> "BottomNavigationDotBar"
          color: Colors.black26,
          items: <BottomNavigationDotBarItem>[
            BottomNavigationDotBarItem(
                icon: IconData(0xe900, fontFamily: 'home'),
                onTap: () {
                  setState(() {
                    currentIndex = 0;
                  });
                }),
            BottomNavigationDotBarItem(
                icon: IconData(0xe900, fontFamily: 'hearth'),
                onTap: () {
                  setState(() {
                    currentIndex = 2;
                  });
                }),
            BottomNavigationDotBarItem(
                icon: IconData(0xe900, fontFamily: 'profile'),
                onTap: () {
                  setState(() {
                    currentIndex = 3;
                  });
                }),
          ]),
    );
  }
}
