import 'package:flutter/material.dart';

class QRCodeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            _printStatus(),
            _printComfort(),
            _printQR(),
          ],
        ),
      ),
    );
  }

  // Supportive Widgets

  Widget _printStatus() {
    final statusInfo = Column(
      children: [
        Text(
          'Negative(-)',
          style: c19StatusStyle,
        ),
        Text('Tested by My Health Lab - 086'),
        Text('On 12th January 2021 At 02:45:56 PM'),
      ],
    );

    return Container(
      //padding: EdgeInsets.only(top: 90),
      child: statusInfo,
    );
  }

  Widget _printComfort() {
    final comfortInfo = Column(
      children: [
        Text(
          '670',
          style: comfortNumberStyle,
        ),
        Text(
          'Comfort',
          style: statusStyle,
        ),
      ],
    );

    return Container(
      //padding: EdgeInsets.only(top: 10),
      child: comfortInfo,
    );
  }

  Widget _printQR() {
    return Image(
      image: AssetImage('assets/image/qr.jpg'),
      fit: BoxFit.scaleDown,
      width: 250,
      height: 250,
    );
  }

  // Styles

  final statusStyle = TextStyle(
    color: Colors.lightGreen,
    fontWeight: FontWeight.w800,
    fontFamily: 'Roboto',
    letterSpacing: 0.5,
    fontSize: 15,
    //height: 2,
  );

  final comfortNumberStyle = TextStyle(
    color: Colors.indigo,
    fontWeight: FontWeight.w800,
    fontFamily: 'Roboto',
    letterSpacing: 0.5,
    fontSize: 35,
    //height: 2,
  );

  final c19StatusStyle = TextStyle(
    color: Colors.indigo,
    fontWeight: FontWeight.w600,
    fontFamily: 'Roboto',
    letterSpacing: 0.5,
    fontSize: 35,
    height: 2,
  );
} // End of QRCodeScreen Class
