import 'package:flutter/material.dart';
import 'qr_code.dart';
import 'vitals.dart';
import 'settings.dart';

class Butterfly extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'QR Code Page',
      theme: ThemeData(
        primaryColor: Colors.blueGrey,
      ),
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PageController _controller = PageController(
    initialPage: 0,
  );

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return PageView(
      controller: _controller,
      scrollDirection: Axis.horizontal,
      children: [
        QRCodeScreen(),
        VitalSignScreen(),
        PrivacyScreen(),
      ],
    );
  }
} // End of _HomePageState Class
