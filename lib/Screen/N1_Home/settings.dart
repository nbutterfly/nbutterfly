import 'package:flutter/material.dart';

/*class PrivacyScreen extends StatefulWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Text('fffff'),
      ),
    );
  }
} // End of VitalSignScreen Class
*/

class PrivacyScreen extends StatefulWidget {
  @override
  _PrivacyScreenState createState() => _PrivacyScreenState();
}

class _PrivacyScreenState extends State<PrivacyScreen> {
  bool _value1 = false;
  bool _value2 = false;

  //we omitted the brackets '{}' and are using fat arrow '=>' instead, this is dart syntax
  void _value1Changed(bool value) => setState(() => _value1 = value);
  void _value2Changed(bool value) => setState(() => _value2 = value);

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('Privacy Settings'),
      ),
      //hit Ctrl+space in intellij to know what are the options you can use in flutter widgets
      body: new Container(
        padding: new EdgeInsets.all(12.0),
        child: new Center(
          child: new Column(
            children: <Widget>[
              new CheckboxListTile(
                value: _value1,
                onChanged: _value1Changed,
                title: new Text('"OFF means OFF" Privacy mode'),
                controlAffinity: ListTileControlAffinity.leading,
                subtitle: new Text('Vitals without refreshing Comfort-Index'),
              ),
              new CheckboxListTile(
                value: _value2,
                onChanged: _value2Changed,
                title: new Text('Opt-in for C-19 Status sharing'),
                controlAffinity: ListTileControlAffinity.leading,
                subtitle: new Text('Share your current Status anonymously.'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
